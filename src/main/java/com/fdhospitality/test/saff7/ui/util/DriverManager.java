package com.fdhospitality.test.saff7.ui.util;

import java.net.URI;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;

public class DriverManager {

	@Autowired
	private URI siteBase;

	private WebDriver driver;

	public URI getSiteBase() {
		return siteBase;
	}

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}
}
