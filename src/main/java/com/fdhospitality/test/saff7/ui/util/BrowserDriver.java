package com.fdhospitality.test.saff7.ui.util;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;

public class BrowserDriver {

	@Autowired
	private DriverManager driverManager;

	@Autowired
	private BrowserFactory browserFactory;

	private static final Logger LOGGER = Logger.getLogger(BrowserDriver.class.getName());

	public WebDriver getCurrentDriver() {
		if (driverManager.getDriver() == null) {
			try {
				driverManager.setDriver(browserFactory.getBrowser());
			} catch (UnreachableBrowserException e) {
				e.printStackTrace();
			} catch (WebDriverException e) {
				e.printStackTrace();
			}
		}
		return driverManager.getDriver();
	}

	public void getPortalPage() {
		loadPage(driverManager.getSiteBase().toString());
	}

	public void close() {
		try {
			driverManager.getDriver().quit();
			driverManager.setDriver(null);
			LOGGER.info("closing the browser");
		} catch (UnreachableBrowserException e) {
			LOGGER.info("cannot close browser: unreachable browser");
		}
	}

	public void loadPage(String url) {
		getCurrentDriver();
		LOGGER.info("Directing browser to:" + url);
		LOGGER.info("try to loadPage [" + url + "]");
		getCurrentDriver().get(url);
	}

	public void reopenAndLoadPage(String url) {
		driverManager.setDriver(null);
		getCurrentDriver();
		loadPage(url);
	}

	public byte[] getScreenshot() {
		return ((TakesScreenshot) driverManager.getDriver()).getScreenshotAs(OutputType.BYTES);
	}

	public WebElement waitForElement(WebElement elementToWaitFor) {
		return waitForElement(elementToWaitFor, null);
	}

	public boolean waitForElementNotToBeShown(WebElement elementToWaitFor) {
		WebDriverWait wait = new WebDriverWait(getCurrentDriver(), 10);
		return wait.until(ExpectedConditions.invisibilityOfElementLocated((By) elementToWaitFor));
	}

	public void waitForPageLoad() {
		getCurrentDriver().manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
	}

	public void implicitWait() {
		getCurrentDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	public WebElement waitForElement(WebElement elementToWaitFor, Integer waitTimeInSeconds) {
		if (waitTimeInSeconds == null) {
			waitTimeInSeconds = 10;
		}

		WebDriverWait wait = new WebDriverWait(getCurrentDriver(), waitTimeInSeconds);
		return wait.until(ExpectedConditions.visibilityOf(elementToWaitFor));
	}

	public String getCurrentURL() {
		return driverManager.getDriver().getCurrentUrl();
	}

	public WebElement getModal() {
		return driverManager.getDriver().switchTo().activeElement();
	}

	public static WebElement getParent(WebElement element) {
		return element.findElement(By.xpath(".."));
	}

	public static List<WebElement> getDropDownOptions(WebElement webElement) {
		Select select = new Select(webElement);
		return select.getOptions();
	}

	public static WebElement getDropDownOption(WebElement webElement, String value) {
		WebElement option = null;
		List<WebElement> options = getDropDownOptions(webElement);
		for (WebElement element : options) {
			if (element.getAttribute("value").equalsIgnoreCase(value)) {
				option = element;
				break;
			}
		}
		return option;
	}

	public WebElement getButtonElement(String buttonDescription) {
		WebElement buttonElement = getCurrentDriver()
				.findElement(By.xpath("//button[contains(.,'" + buttonDescription + "')]"));
		return buttonElement;
	}

	public List<WebElement> getAllButtonElements() {
		return getCurrentDriver().findElements(By.cssSelector("button[type=button]"));
	}

	public List<WebElement> getAllCheckboxElements() {
		return getCurrentDriver().findElements(By.xpath("//input[@type='checkbox']"));
	}

	public WebElement getLinkElement(String linkDescription) {
		WebElement linkElement = getCurrentDriver().findElement(By.partialLinkText(linkDescription));
		return linkElement;
	}

	public List<WebElement> findAllLinks() {

		List<WebElement> elementList = new ArrayList<WebElement>();
		elementList = getCurrentDriver().findElements(By.tagName("a"));

		List<WebElement> finalList = new ArrayList<WebElement>();
		for (WebElement element : elementList) {
			finalList.add(element);
		}

		return finalList;
	}

	public String isLinkBroken(URL url) throws Exception {

		String response = "";
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();

		try {
			connection.connect();
			response = connection.getResponseMessage();
			connection.disconnect();
			return response;
		}

		catch (Exception exp) {
			return exp.getMessage();
		}
	}
}
