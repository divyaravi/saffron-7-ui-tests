package com.fdhospitality.test.saff7.ui.containers;

import org.openqa.selenium.support.PageFactory;

import com.fdhospitality.test.saff7.ui.util.BrowserDriver;

/**
 * Created by sager on 19/12/2016.
 */
public class LocatorsFactory {
    /**
     * Factory method used by Spring to create instance of Locators class
     *
     * @param browserDriver
     * @return Instance of Locators
     */
    public static Locators createInstance(BrowserDriver browserDriver) {
        return PageFactory.initElements(browserDriver.getCurrentDriver(), Locators.class);
    }
}
