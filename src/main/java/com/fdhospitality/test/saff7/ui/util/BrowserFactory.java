package com.fdhospitality.test.saff7.ui.util;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.springframework.beans.factory.annotation.Autowired;

import com.fdhospitality.test.saff7.ui.constants.Browsers;
import com.fdhospitality.test.saff7.ui.run.BrowserProperties;

public class BrowserFactory {

	@Autowired
	private BrowserProperties browserProp;

	@Autowired
	private DriverManager driverManager;

	public WebDriver getBrowser() {
		Browsers browser;

		if (browserProp.getBrowserName() == null) {
			browser = Browsers.FIREFOX;
		} else {
			browser = Browsers.browserForName(browserProp.getBrowserName());
		}
		switch (browser) {
		case CHROME:
			driverManager.setDriver(createChromeDriver());
			break;
		case SAFARI:
			driverManager.setDriver(createSafariDriver());
			break;
		case EDGE:
			driverManager.setDriver(createEdgeDriver());
			break;
		case FIREFOX:
		default:
			driverManager.setDriver(createFirefoxDriver());
			break;
		}
		addAllBrowserSetup(driverManager.getDriver());
		return driverManager.getDriver();
	}

	private static WebDriver createSafariDriver() {
		return new SafariDriver();
	}

	private static WebDriver createChromeDriver() {
		System.setProperty("webdriver.chrome.driver", "src/main/resources/drivers/chromedriver.exe");
		return new ChromeDriver();
	}

	private static WebDriver createEdgeDriver() {
		System.setProperty("webdriver.edge.driver", "src/main/resources/drivers/MicrosoftWebDriver.exe");
		return new EdgeDriver();
	}

	private static WebDriver createFirefoxDriver() {
		System.setProperty("webdriver.firefox.marionette", "src/main/resources/drivers/geckodriver.exe");
		return new FirefoxDriver();
	}

	private static void addAllBrowserSetup(WebDriver driver) {
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		/*
		 * driver.manage().window().setPosition(new Point(0, 0));
		 * java.awt.Dimension screenSize =
		 * Toolkit.getDefaultToolkit().getScreenSize(); Dimension dim = new
		 * Dimension((int) screenSize.getWidth(), (int) screenSize.getHeight());
		 */
		driver.manage().window().maximize();
	}
}
