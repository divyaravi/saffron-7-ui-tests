package com.fdhospitality.test.saff7.ui.stepDefinition;

import com.fdhospitality.test.saff7.ui.containers.Locators;
import com.fdhospitality.test.saff7.ui.containers.LocatorsFactory;
import com.fdhospitality.test.saff7.ui.util.BrowserDriver;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import static org.testng.AssertJUnit.assertTrue;

public class S7Login implements InitializingBean {

	private Locators locator;

	@Autowired
	private BrowserDriver browserDriver;

	/**
	 * Called by Spring when all properties have been set.
	 *
	 * @throws Exception
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		locator = LocatorsFactory.createInstance(browserDriver);
	}

	public Locators getLocator() {
		return this.locator;
	}

	@When("^I click on Aged Care database as FDH user$")
	public void i_click_on_aged_care_link_as_FDH() {
		getLocator().poloDbAsFdh.click();
	}

	@Then("^I expect to be navigated to the home page of Aged Care$")
	public void i_expect_to_be_navigated_to_the_home_page_of_aged_care() throws Throwable {
		browserDriver.implicitWait();
		System.out.println("URL:" + browserDriver.getCurrentDriver().getCurrentUrl());
		try {
			browserDriver.waitForElement(getLocator().toggleMenu);
		} catch (Exception e) {
			browserDriver.getCurrentDriver().navigate().refresh();
			browserDriver.implicitWait();
		}
		assertTrue(browserDriver.getCurrentDriver().getCurrentUrl().contains("saff7test"));
	}

	@Then("^I expect username \"([^\"]*)\" to be on the left hand side$")
	public void i_expect_username_to_be_on_the_left_hand_side(String username) throws Throwable {
		assertTrue(getLocator().usernameDashboard.getText().contains(username));
	}

	@Then("^I expect \"([^\"]*)\" database name$")
	public void i_expect_polo_s7_database_name(String dbName) throws Throwable {
		assertTrue(getLocator().dbNameDashboard.getText().contains(dbName));
	}

	@Then("^I expect About Saffron screen to be displayed$")
	public void i_expect_About_Saffron_screen_to_be_displayed() throws Throwable {
		browserDriver.waitForPageLoad();
		browserDriver.getModal();
		browserDriver.waitForElement(getLocator().aboutSaffronHeader);
		assertTrue(getLocator().aboutSaffronHeader.getText().contains("About"));
	}

	@Then("^I expect latest version of saffron to be shown as \"([^\"]*)\"$")
	public void i_expect_latest_version_to_be_shown(String version) throws Throwable {
		try {
			assertTrue((browserDriver.waitForElementNotToBeShown(getLocator().progressLoaderCube)) == false);

			if (getLocator().progressLoaderCube.isDisplayed()) {
				System.out.println("Error: Loader cube is shown continuously ");
			}
		} catch (Exception e) {
			browserDriver.waitForElement(getLocator().s7Version);
			assertTrue(getLocator().s7Version.getText().contains(version));
		}
	}

	@Then("^I expect old version of Saffron to be shown as \"([^\"]*)\"$")
	public void i_expect_old_version_to_be_shown(String version) throws Throwable {
		assertTrue(getLocator().a1Version.getText().contains(version));
	}

	@Then("^I expect database version to be shown as \"([^\"]*)\"$")
	public void i_expect_database_version_to_be_shown(String version) throws Throwable {
		assertTrue(getLocator().databaseVersion.getText().contains(version));
	}

	@Then("^I expect License Expiry Date to be shown as \"([^\"]*)\"$")
	public void i_expect_License_Expiry_Date_to_be_shown(String date) throws Throwable {
		assertTrue(getLocator().licenseExpiryDate.getText().contains(date));
	}

	@Then("^I expect number of concurrent users to be shown as \"([^\"]*)\"$")
	public void i_expect_number_of_concurrent_users_to_be_shown(String concurrentUserNumber) throws Throwable {
		assertTrue(getLocator().concurrentUsers.getText().contains(concurrentUserNumber));
	}

	@Then("^I expect About Saffron screen to be closed$")
	public void i_expect_About_Saffron_screen_to_be_closed() throws Throwable {
		assertTrue(getLocator().mainMenuLink.isEnabled());
	}

	@Then("^I expect main menu to be shown open$")
	public void i_expect_main_menu_to_be_shown_open() throws Throwable {
		assertTrue(getLocator().mainMenuLink.isDisplayed());
	}

}
