package com.fdhospitality.test.saff7.ui.run;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;


@CucumberOptions(
        features = { "classpath:features/" },
        glue = { "com.fdhospitality.test.saff7.ui.stepDefinition" },
        plugin = {
                "html:target/cucumber-reports/test-report",
                "json:target/cucumber-reports/test-json-report.json",
                "com.fdhospitality.test.saff7.ui.util.EmailReporter:test.properties"
        },
        //  tags = { "@loginportalintegration" },
        monochrome = true)
public class TestRunner extends AbstractTestNGCucumberTests {
    // Empty
}
