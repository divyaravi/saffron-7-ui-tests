package com.fdhospitality.test.saff7.ui.stepDefinition;

import com.fdhospitality.test.saff7.ui.containers.Locators;
import com.fdhospitality.test.saff7.ui.containers.LocatorsFactory;
import com.fdhospitality.test.saff7.ui.util.BrowserDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import static org.testng.AssertJUnit.*;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

public class CoreSteps implements InitializingBean {

	@Autowired
	private BrowserDriver browserDriver;

	private Locators locator;

	/**
	 * Called by Spring when all properties have been set.
	 *
	 * @throws Exception
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		locator = LocatorsFactory.createInstance(browserDriver);
	}

	public Locators getLocator() {
		return this.locator;
	}

	@Then("^I expect \"([^\"]*)\" button to be present and clickable$")
	public void i_expect_button_to_be_present_and_clickable(String buttonDescription) {
		browserDriver.waitForElement(browserDriver.getButtonElement(buttonDescription));
		assertTrue(browserDriver.getButtonElement(buttonDescription).isDisplayed());
		assertTrue(browserDriver.getButtonElement(buttonDescription).isEnabled());
	}

	@Then("^I expect \"([^\"]*)\" link to be present and clickable$")
	public void i_expect_link_to_be_present_and_clickable(String linkDescription) {
		assertTrue(browserDriver.getLinkElement(linkDescription).isDisplayed());
		assertTrue(browserDriver.getLinkElement(linkDescription).isEnabled());
	}

	@When("^I click on \"([^\"]*)\" button$")
	public void i_click_on_button(String buttonDescription) throws InterruptedException {
		WebElement element = browserDriver.getButtonElement(buttonDescription);
		element.sendKeys(Keys.ENTER);
	}

	@When("^I click on \"([^\"]*)\" link$")
	public void i_click_on_link(String linkDescription) {
		browserDriver.waitForElement(browserDriver.getLinkElement(linkDescription));
		browserDriver.getLinkElement(linkDescription).click();
	}

	@Given("^I'm logged in with username \"([^\"]*)\" and password \"([^\"]*)\"$")
	public void i_m_logged_in_with_username_and_password(String user, String pswd) throws Exception {
		browserDriver.waitForElement(getLocator().username, 15);
		getLocator().username.sendKeys(user);
		getLocator().passwordCSS.sendKeys(pswd);
		getLocator().passwordCSS.sendKeys(Keys.ENTER);
		System.out.println("\nLogged in as " + user);
		System.out.println("\nOn the databases page");
		browserDriver.waitForPageLoad();
	}

	@Then("^I expect \"([^\"]*)\" button to be present and disabled$")
	public void i_expect_button_to_be_present_and_disabled(String button) throws Throwable {
		assertTrue(browserDriver.getButtonElement(button).isDisplayed());
		browserDriver.waitForElement(browserDriver.getButtonElement(button));
		assertFalse(browserDriver.getButtonElement(button).isEnabled());
	}

	@When("^I click on main menu toggle$")
	public void i_click_on_main_menu_toggle() throws Throwable {
		Actions builder = new Actions(browserDriver.getCurrentDriver());
		builder.moveToElement(getLocator().toggleMenu).build().perform();
		getLocator().toggleMenu.click();
	}

	@Then("^I expect not more than (\\d+) menu options$")
	public void i_expect_not_more_than_menu_options(int menuCount) throws Throwable {
		List<WebElement> options = getLocator().menuOptions.findElements(getLocator().getListTag());
		assertEquals(options.size(), menuCount);
	}

	@Then("^I expect more than (\\d+) menu options$")
	public void i_expect_more_than_menu_options(int menuCount) throws Throwable {
		List<WebElement> options = getLocator().menuOptions.findElements(getLocator().getListTag());
		assertTrue(options.size() >= menuCount);
	}

}
