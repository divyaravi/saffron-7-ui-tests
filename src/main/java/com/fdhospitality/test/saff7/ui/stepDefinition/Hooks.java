package com.fdhospitality.test.saff7.ui.stepDefinition;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.WebDriverException;
import org.springframework.beans.factory.annotation.Autowired;

import com.fdhospitality.test.saff7.ui.util.BrowserDriver;

public class Hooks {

	@Autowired
	private BrowserDriver browserDriver;

	public BrowserDriver getBrowserDriver() {
		return this.browserDriver;
	}

	@Before
	public void beforeScenario() {
		getBrowserDriver().getPortalPage();
	}

	@After
	public void afterScenario(Scenario scenario) throws Exception {
		if (scenario.isFailed()) {
			try {
				scenario.write("Current Page URL is " + getBrowserDriver().getCurrentURL());
				byte[] screenshot = getBrowserDriver().getScreenshot();
				scenario.embed(screenshot, "image/png");
			} catch (WebDriverException somePlatformsDontSupportScreenshots) {
				System.err.println(somePlatformsDontSupportScreenshots.getMessage());
			}
		}
		getBrowserDriver().close();
	}
}
