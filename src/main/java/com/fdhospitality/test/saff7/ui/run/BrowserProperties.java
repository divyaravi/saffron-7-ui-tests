package com.fdhospitality.test.saff7.ui.run;

public class BrowserProperties {

	private String browserName;

	public String getBrowserName() {
		return browserName;
	}

	public void setBrowserName(String browserName) {
		this.browserName = browserName;
	}

}
