package com.fdhospitality.test.saff7.ui.stepDefinition;

import com.fdhospitality.test.saff7.ui.containers.Locators;
import com.fdhospitality.test.saff7.ui.containers.LocatorsFactory;
import com.fdhospitality.test.saff7.ui.util.BrowserDriver;

import cucumber.api.java.en.Then;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import static org.testng.AssertJUnit.assertTrue;

public class Dashboard implements InitializingBean {

	private Locators locator;

	@Autowired
	private BrowserDriver browserDriver;

	/**
	 * Called by Spring when all properties have been set.
	 *
	 * @throws Exception
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		locator = LocatorsFactory.createInstance(browserDriver);
	}

	public Locators getLocator() {
		return this.locator;
	}

	@Then("^I expect main menu to be on the left$")
	public void i_expect_main_menu_to_be_shown_open() throws Throwable {
		// Checking the xpath of main menu tab will ensure main menu is
		// positioned left
		assertTrue(getLocator().mainMenuTab.isEnabled());
	}

}
