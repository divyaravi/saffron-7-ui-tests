package com.fdhospitality.test.saff7.ui.containers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Locators {

	@FindBy(css = "input[ng-model=username]")
	public WebElement username;

	@FindBy(xpath = ".//*[@id='toggle-side-nav']")
	public WebElement toggleMenu;

	@FindBy(partialLinkText = "Home")
	public WebElement homeLink;

	@FindBy(partialLinkText = "Main Menu")
	public WebElement mainMenuLink;

	@FindBy(xpath = ".//*[@id='app']/div/div/form/div/div[1]/div[2]/input")
	public WebElement password;

	@FindBy(css = "input[type=password]")
	public WebElement passwordCSS;

	@FindBy(xpath = ".//*[@id='app']/div/div/form/div/div[1]/button[1]")
	public WebElement loginButton;

	@FindBy(xpath = ".//div[span='POLO_S7_A1']")
	public WebElement a1DB;

	@FindBy(xpath = ".//div[span='POLO_S7_CI_AutoTest']")
	public WebElement poloDB;

	@FindBy(xpath = ".//*[@id='app']/div/div/form/div/div[1]/div/div[8]/div/div[2]/button")
	public WebElement poloDbAsFdh;

	@FindBy(xpath = ".//*[@id='app']/div/div/div[2]/div/a")
	public WebElement loginDropdown;

	@FindBy(xpath = ".//*[@id='TourStep1']/div/h5")
	public WebElement usernameDashboard;

	@FindBy(xpath = ".//*[@id='TourStep1']/div/small")
	public WebElement dbNameDashboard;

	@FindBy(xpath = ".//header/div")
	public WebElement aboutSaffronHeader;

	@FindBy(className = "loader-cube-grid")
	public WebElement progressLoaderCube;

	@FindBy(xpath = ".//*[@id='navigation']/div/section/div/div/div[1]/div/table/tbody/tr[1]/td")
	public WebElement s7Version;

	@FindBy(xpath = ".//*[@id='navigation']/div/section/div/div/div[1]/div/table/tbody/tr[2]/td")
	public WebElement a1Version;

	@FindBy(xpath = ".//*[@id='navigation']/div/section/div/div/div[1]/div/table/tbody/tr[3]/td")
	public WebElement databaseVersion;

	@FindBy(xpath = ".//*[@id='navigation']/div/section/div/div/div[2]/div/table/tbody/tr[1]/td")
	public WebElement licenseExpiryDate;

	@FindBy(xpath = ".//*[@id='navigation']/div/section/div/div/div[2]/div/table/tbody/tr[2]/td")
	public WebElement concurrentUsers;

	@FindBy(xpath = ".//*[@id='navigation']/div/nav/div[1]")
	public WebElement menuTabsDiv;

	@FindBy(xpath = ".//*[@id='navigation']/div/nav/div[1]/a[1]")
	public WebElement mainMenuTab;

	@FindBy(xpath = ".//*[@id='navigation']/div/nav/div[2]/div/ul")
	public WebElement menuOptions;

	By linkTag = By.tagName("a");

	public By getLinkTag() {
		return linkTag;
	}

	public void setLinkTag(By linkTag) {
		this.linkTag = linkTag;
	}

	By listTag = By.tagName("li");

	public By getListTag() {
		return listTag;
	}

	public void setListTag(By listTag) {
		this.listTag = listTag;
	}

}
