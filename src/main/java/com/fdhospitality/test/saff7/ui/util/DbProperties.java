package com.fdhospitality.test.saff7.ui.util;

public class DbProperties {

	private String dbUrl;

	public String getDbUrl() {
		return dbUrl;
	}

	public void setDbUrl(String dbUrl) {
		this.dbUrl = dbUrl;
	}

}
