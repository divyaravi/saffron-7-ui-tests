package com.fdhospitality.test.saff7.ui.util;

import gherkin.formatter.Formatter;
import gherkin.formatter.Reporter;
import gherkin.formatter.model.*;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.fdhospitality.test.saff7.ui.verifyMethods.Verify;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Class implementing Gherkin Formatter and Reporter interfaces that stores
 * errors as the tests run and emails them out at the end.
 */
public class EmailReporter implements Reporter, Formatter {

	private ErrorHolder errors = new ErrorHolder();

	private EmailProperties emailProperties = null;

	private DbProperties dbProperties = null;

	Connection conn = null;
	Statement stmt = null;

	private String featureName;
	private String scenarioName;
	private String stepName;

	public EmailReporter(File propertiesFile) {
		loadProperties(propertiesFile);
	}

	private void loadProperties(File propertiesFile) {
		Properties prop = new Properties();
		InputStream input = null;

		try {
			input = EmailReporter.class.getClassLoader().getResourceAsStream(propertiesFile.getName());
			if (input == null) {
				System.err.println("Sorry, unable to find " + propertiesFile);
				return;
			}

			// load a properties file from class path, inside static method
			prop.load(input);

			emailProperties = new EmailProperties();

			emailProperties.setEmailFrom(prop.getProperty("email.from"));
			emailProperties.setEmailTo(prop.getProperty("email.to"));
			emailProperties.setSmtpUsername(prop.getProperty("smtp.username"));
			emailProperties.setSmtpPassword(prop.getProperty("smtp.password"));
			emailProperties.setSmtpHost(prop.getProperty("smtp.host"));
			emailProperties.setSmtpPort(prop.getProperty("smtp.port"));

			dbProperties = new DbProperties();
			dbProperties.setDbUrl(prop.getProperty("db.url"));

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////
	// REPORTER OVERRIDES
	//

	@Override
	public void before(Match match, Result result) {
		// EMPTY

		// System.out.println("REPORTER: Before Match: " + match.getLocation() +
		// "; Status = " + result.getStatus());
	}

	@Override
	public void result(Result result) {
		// System.out.println("REPORTER: Result status: " + result.getStatus());

		if (result.getStatus().equals(Result.FAILED)) {
			errors.addError(scenarioName, stepName, featureName);
		}
	}

	@Override
	public void after(Match match, Result result) {
		// EMPTY

		// System.out.println("REPORTER: After Match: " + match.getLocation() +
		// "; Status = " + result.getStatus());
	}

	@Override
	public void match(Match match) {
		stepName = match.getLocation();

		// System.out.println("REPORTER: Match: " + match.getLocation());
	}

	@Override
	public void embedding(String s, byte[] bytes) {
		// EMPTY
	}

	@Override
	public void write(String s) {
		// EMPTY
	}

	//////////////////////////////////////////////////////////////////////////
	// FORMATTER OVERRIDES
	//

	@Override
	public void syntaxError(String s, String s1, List<String> list, String s2, Integer integer) {
		// EMPTY
	}

	@Override
	public void uri(String s) {
		// EMPTY

		// System.out.println("FORMATTER: URI: " + s);
	}

	@Override
	public void feature(Feature feature) {
		featureName = feature.getName();

		// System.out.println("FORMATTER: Feature: " + feature.getName());
	}

	@Override
	public void scenarioOutline(ScenarioOutline scenarioOutline) {
		// EMPTY

		// System.out.println("FORMATTER: Scenario Outline: " +
		// scenarioOutline.getName());
	}

	@Override
	public void examples(Examples examples) {
		// EMPTY
	}

	@Override
	public void startOfScenarioLifeCycle(Scenario scenario) {
		// System.out.println("FORMATTER: Start of Scenario Lifecycle Outline: "
		// + scenario.getName());
	}

	@Override
	public void background(Background background) {
		// EMPTY
	}

	@Override
	public void scenario(Scenario scenario) {
		scenarioName = scenario.getName();

		// System.out.println("FORMATTER: Scenario: " + scenario.getName());
	}

	@Override
	public void step(Step step) {
		// EMPTY

		// System.out.println("FORMATTER: Step: " + step.getName());
	}

	@Override
	public void endOfScenarioLifeCycle(Scenario scenario) {
		// EMPTY

		// System.out.println("FORMATTER: End of Scenario Lifecycle Outline: " +
		// scenario.getName());
	}

	@Override
	public void done() {
		try {
			MimeMessage message = new MimeMessage(getMailSession());
			message.setFrom(new InternetAddress(emailProperties.getEmailFrom()));
			InternetAddress[] iAddressArray = InternetAddress.parse(emailProperties.getEmailTo());
			message.setRecipients(Message.RecipientType.TO, iAddressArray);

			String currentTimeStamp = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
			message.setSubject("Saffron 7 UI Test Report - " + currentTimeStamp);

			if (errors.hasErrors()) {
				String failureText = getFailureText();
				message.setText(failureText);
			} else {
				message.setText("Tests ran successfully!");
			}

			Transport.send(message);
		} catch (MessagingException ex) {
			ex.printStackTrace();
		}

		// Delete test data for next test run:

		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
			conn = DriverManager.getConnection(dbProperties.getDbUrl());
			stmt = conn.createStatement();
			Verify delete = new Verify();
			delete.deleteTestNames(stmt);
			conn.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void close() {
		// EMPTY

		// System.out.println("FORMATTER: Close");
	}

	@Override
	public void eof() {
		// EMPTY

		// System.out.println("FORMATTER: EOF");
	}

	private Session getMailSession() {
		Properties properties = System.getProperties();
		properties.put("mail.smtp.host", emailProperties.getSmtpHost());
		properties.put("mail.smtp.socketFactory.port", emailProperties.getSmtpPort());
		properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.port", emailProperties.getSmtpPort());

		Authenticator auth = new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(emailProperties.getSmtpUsername(), emailProperties.getSmtpPassword());
			}
		};

		return Session.getDefaultInstance(properties, auth);
	}

	private String getFailureText() {
		StringBuilder buff = new StringBuilder();
		System.out.println("\nFailures:\n\n");
		for (Map.Entry<String, List<String>> entry2 : errors.getErrors()) {
			buff.append(entry2.getKey() + " : ").append("\n\n");

			for (String stepName : entry2.getValue()) {
				buff.append("\t").append(stepName).append("\n");
			}

			buff.append("\n");
		}

		System.out.println(buff.toString());

		return buff.toString();
	}
}
