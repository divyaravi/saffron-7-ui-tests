package com.fdhospitality.test.saff7.ui.util;

import java.util.*;

/**
 * Class holding Cucumber test failures
 */
public class ErrorHolder {

	private Map<String, List<String>> errors = new HashMap<>();

	public void addError(String scenarioName, String stepName, String featureName) {

		System.out.println("Adding error for feature \"" + featureName + "\", scenario \"" + scenarioName
				+ "\" and step \"" + stepName + "\"");

		List<String> scenarioErrors = errors.get(featureName + " - " + scenarioName);

		if (scenarioErrors == null) {
			scenarioErrors = new ArrayList<>();
			errors.put(featureName + " - " + scenarioName, scenarioErrors);
		}

		scenarioErrors.add(stepName);
	}

	public Set<Map.Entry<String, List<String>>> getErrors() {
		return errors.entrySet();
	}

	public boolean hasErrors() {
		return errors.size() > 0;
	}
}
