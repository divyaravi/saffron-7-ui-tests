#Author: divya.ravi@fdhospitality.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""

  
@loginpage
Feature: Login and home page
  This is to test S7 login from Login Portal
  
  Background:  Portal to aged care   
    Given I'm logged in with username "divya" and password "5Trawb3rry7"
    When I click on Aged Care database as FDH user
    Then I expect to be navigated to the home page of Aged Care

	@loginportalintegration
  Scenario: Select Aged-Care database
		Then I expect username "Divya" to be on the left hand side
		And I expect "POLO_S7_CI_AutoTest" database name
	  
	@about
	Scenario: About Saffron
	  When I click on main menu toggle
	  Then I expect "About Saffron" link to be present and clickable
	  When I click on "About Saffron" link
	  Then I expect About Saffron screen to be displayed
	  And I expect latest version of saffron to be shown as "1.0.0."
	  And I expect old version of Saffron to be shown as "6.1500."
	  And I expect database version to be shown as "03.81.0A"
	  And I expect License Expiry Date to be shown as "30th Sep 2020"
	  And I expect number of concurrent users to be shown as "0"
	  And I expect "Close" link to be present and clickable
	  When I click on "Close" link
	  Then I expect About Saffron screen to be closed
	  And I expect main menu to be shown open

		