#Author: divya.ravi@fdhospitality.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""

  
@dashboard
Feature: Dashboard
  This is to test S7 dashboard main menu
  
  Background:  Portal to aged care   
    Given I'm logged in with username "divya" and password "5Trawb3rry7"
    When I click on Aged Care database as FDH user
    Then I expect to be navigated to the home page of Aged Care
	  
	@dashboardmenu
	Scenario: Display main menu and job menu in navigation menu
	  When I click on main menu toggle
	  Then I expect "Main Menu" link to be present and clickable
	  And I expect "TestJobMenu" link to be present and clickable
	  
	@tabpositions
	Scenario: Menu tabs positioning
	  When I click on main menu toggle
	  Then I expect main menu to be on the left
	  
	@mainmenu
	Scenario: Main menu options
	  When I click on main menu toggle
	  Then I expect not more than 3 menu options
	  And I expect "Operational Modules" link to be present and clickable
	  And I expect "Reference Information" link to be present and clickable
	  And I expect "System Maintenance" link to be present and clickable
	  
 	@mainmenu
	Scenario: Main menu heirarchy
	  When I click on main menu toggle
	  And I click on "Operational Modules" link
	  Then I expect "Card Reading" link to be present and clickable
	  When I click on "Operational Modules" link
	  Then I expect not more than 3 menu options
	  
	@jobmenu
	Scenario: Job menu options
	  When I click on main menu toggle
	  And I click on "TestJobMenu" link
	  Then I expect more than 3 menu options
	  And I expect "Barcodes" link to be present and clickable


		